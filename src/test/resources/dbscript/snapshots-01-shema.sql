﻿CREATE SCHEMA IF NOT EXISTS `bluebik_test` DEFAULT CHARACTER SET utf8 ;
USE `bluebik_test` ;

  -- -----------------------------------------------------
-- Table `MUSCLE_CAR`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `muscle_car` (
  `CAR_ID` BIGINT(22) NOT NULL AUTO_INCREMENT COMMENT 'RUNNING NUMBER',
  `CAR_BRAND` VARCHAR(50) NOT NULL COMMENT 'Car Brand',
  `CAR_MODEL` VARCHAR(50) NOT NULL COMMENT 'Car Model',
  `HORSEPOWER` VARCHAR(50) NOT NULL COMMENT 'Car horsepower',
  `CAR_ENGINE` VARCHAR(50) NOT NULL COMMENT 'Car CAR_ENGINE',
  PRIMARY KEY (`CAR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;