package com.bluebik.car.service.musclecar;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.bluebik.car.repository.MuscleCarRepository;
import com.bluebik.car.repository.domain.MuscleCar;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
public class MuscleCarServiceTestCase {
	

	@Autowired
	public MuscleCarService muscleCarService;
	
	@Autowired
	public MuscleCarRepository muscleCarRepository;
	

	@Test
	public void successWhenGetCar() {
		// given
		final Long carId = 1L;

		// when
		MuscleCar result = muscleCarService.getCar(carId);

		// then
		assertNotNull(result);
		assertEquals(result.getCarId(), carId);
	}

	@Test
	public void shouldThrowIllegalArgumentExceptionIfCarIdIsNullWhenGetCar() {
		// given
		final Long carId = null;

		try {
			// when
			muscleCarService.getCar(carId);
			fail("IllegalArgumentException not thrown.");

		} catch (IllegalArgumentException e) {
			// then
			assertThat(e).isInstanceOf(IllegalArgumentException.class).hasMessage("ID can not be null.");
		}
	}

	@Test
	public void shouldThrowIllegalArgumentExceptionIfCarIdIsLessThan0WhenGetCar() {
		// given
		final Long carId = -1L;

		try {
			// when
			muscleCarService.getCar(carId);
			fail("IllegalArgumentException not thrown.");

		} catch (IllegalArgumentException e) {
			// then
			assertThat(e).isInstanceOf(IllegalArgumentException.class).hasMessage("ID can not be 0 or less than 0.");
		}
	}

	@Test
	public void successWhenRemoveCarFromList() {
		// given
		final Long carId = 1L;

		// when
		muscleCarService.removeCarFromList(carId);
		
		//then
		assertNull(muscleCarRepository.findOne(carId));
	}
	
	@Test
	public void shouldThrowIllegalArgumentExceptionIfCarIdIsNullWhenRemoveCarFromList() {
		// given
		final Long carId = null;

		try {
			// when
			muscleCarService.removeCarFromList(carId);
			fail("IllegalArgumentException not thrown.");

		} catch (IllegalArgumentException e) {
			// then
			assertThat(e).isInstanceOf(IllegalArgumentException.class).hasMessage("ID can not be null.");
		}
	}

	@Test
	public void shouldThrowIllegalArgumentExceptionIfCarIdIsLessThan0WhenRemoveCarFromList() {
		// given
		final Long carId = -1L;

		try {
			// when
			muscleCarService.removeCarFromList(carId);
			fail("IllegalArgumentException not thrown.");

		} catch (IllegalArgumentException e) {
			// then
			assertThat(e).isInstanceOf(IllegalArgumentException.class).hasMessage("ID can not be 0 or less than 0.");
		}
	}
	
	@Test
	public void listOfMuscleCarWhenListAllCars() {
		// given

		// when
		List<MuscleCar> result = muscleCarService.getAllCars();

		// then
		assertNotNull(result);
		assertThat(result.size()).isGreaterThan(0);
	}
	
	@Test
	public void successWhenAddCarToList() {
		// given
		MuscleCar muscleCar = new MuscleCar("Toyota","Camry","194","XV40"); 
		
		// when
		MuscleCar persisted = muscleCarService.addCarToList(muscleCar);
		
		// then 
		assertNotNull(persisted.getCarId());
		assertEquals(persisted.getCarBrand(), muscleCar.getCarBrand());
		assertEquals(persisted.getCarModel(), muscleCar.getCarModel());
		assertEquals(persisted.getHorsepower(), muscleCar.getHorsepower());
		assertEquals(persisted.getCarEngine(), muscleCar.getCarEngine());
	}
	
	@Test
	public void shouldThrowIllegalArgumentExceptionIfMuscleCarIsNullWhenAddCarToList() {
		// given
		MuscleCar muscleCar = null; 

		try {
			// when
			muscleCarService.addCarToList(muscleCar);
			fail("IllegalArgumentException not thrown.");

		} catch (IllegalArgumentException e) {
			// then
			assertThat(e).isInstanceOf(IllegalArgumentException.class).hasMessage("The passed object can not be null.");
		}
	}
	
	@Test
	public void successWhenUpdateCarFromList() {
		// given
		final Long carId = 1L;
		MuscleCar muscleCar = muscleCarService.getCar(carId);
		muscleCar.setCarBrand("Change Band");
		muscleCar.setCarEngine("Change Engine");
		
		// when
		MuscleCar persisted = muscleCarService.updateCarFromList(carId,muscleCar);
		
		// then 
		assertEquals(persisted.getCarId(), muscleCar.getCarId());
		assertEquals(persisted.getCarBrand(), muscleCar.getCarBrand());
		assertEquals(persisted.getCarModel(), muscleCar.getCarModel());
		assertEquals(persisted.getHorsepower(), muscleCar.getHorsepower());
		assertEquals(persisted.getCarEngine(), muscleCar.getCarEngine());
	}
	
	@Test
	public void shouldThrowIllegalArgumentExceptionIfCarIdIsNullWhenUpdateCarFromList() {
		// given
		final Long carId = null;
		MuscleCar muscleCar = new MuscleCar();

		try {
			// when
			muscleCarService.updateCarFromList(carId, muscleCar);
			fail("IllegalArgumentException not thrown.");

		} catch (IllegalArgumentException e) {
			// then
			assertThat(e).isInstanceOf(IllegalArgumentException.class).hasMessage("ID can not be null.");
		}
	}
	
	@Test
	public void shouldThrowIllegalArgumentExceptionIfCarIdIsLessThanWhenUpdateCarFromList() {
		// given
		final Long carId = -1L;
		MuscleCar muscleCar = new MuscleCar();

		try {
			// when
			muscleCarService.updateCarFromList(carId, muscleCar);
			fail("IllegalArgumentException not thrown.");

		} catch (IllegalArgumentException e) {
			// then
			assertThat(e).isInstanceOf(IllegalArgumentException.class).hasMessage("ID can not be 0 or less than 0.");
		}
	}
	
	
	@Test
	public void shouldThrowIllegalArgumentExceptionIfMuscleCarIsNullThanWhenUpdateCarFromList() {
		// given
		final Long carId = 1L;
		MuscleCar muscleCar = null;

		try {
			// when
			muscleCarService.updateCarFromList(carId, muscleCar);
			fail("IllegalArgumentException not thrown.");

		} catch (IllegalArgumentException e) {
			// then
			assertThat(e).isInstanceOf(IllegalArgumentException.class).hasMessage("The passed object can not be null.");
		}
	}
	
	
	
}
