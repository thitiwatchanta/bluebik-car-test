package com.bluebik.car.musclecar;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bluebik.car.repository.domain.MuscleCar;
import com.bluebik.car.service.musclecar.MuscleCarService;

/**
 * Copyright © 2016 Bluebik Group. Created by khakhanat on 24/10/2017 AD.
 */

@RestController
@RequestMapping(value = "/api/cars")
public class MuscleCarController {

	@Autowired
	private MuscleCarService muscleCarService;

	@RequestMapping(value = "/get-car/{id}", method = RequestMethod.GET)
	public MuscleCar getMuscleCar(@PathVariable("id") Long id) {
		return muscleCarService.getCar(id);
	}

	@RequestMapping(value = "/delete-car/{id}", method = RequestMethod.DELETE)
	public void deleteMuscleCar(@PathVariable("id") Long id) {
		muscleCarService.removeCarFromList(id);
	}

	@RequestMapping(value = "/add-car", method = RequestMethod.POST)
	public void addCarToList(@RequestBody MuscleCar muscleCar) {
		muscleCarService.addCarToList(muscleCar);
	}

	@RequestMapping(value = "/cars", method = RequestMethod.GET)
	public List<MuscleCar> getAllCars() {
		return muscleCarService.getAllCars();
	}

	@RequestMapping(value = "/update-car/{id}", method = RequestMethod.POST)
	public void updateCar(@PathVariable("id") Long id, @RequestBody MuscleCar muscleCar) {
		muscleCarService.updateCarFromList(id, muscleCar);
	}

}
