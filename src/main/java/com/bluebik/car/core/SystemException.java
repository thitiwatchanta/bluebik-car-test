package com.bluebik.car.core;

public class SystemException extends RuntimeException {
	
	private static final long serialVersionUID = 3393775860252585874L;

	public SystemException(String s) {
		super(s);
	}

	public SystemException(String s, Throwable throwable) {
		super(s, throwable);
	}
}
