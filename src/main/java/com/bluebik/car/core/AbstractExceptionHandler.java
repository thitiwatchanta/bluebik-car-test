package com.bluebik.car.core;

public abstract class AbstractExceptionHandler {

	public static <T> T checkResourceFound(final T resource) {
		if (resource == null) {
			throw new SystemException("ID doesn't exist.");
		}
		return resource;
	}

	public static void validatePassedObject(final Long id) {
		
		if(id == null) {
			throw new IllegalArgumentException("ID can not be null.");
		}
		
		if (id <= 0) {
			throw new IllegalArgumentException("ID can not be 0 or less than 0.");
		}
	}

	public static <T> void validatePassedObject(final T object) {
		if (object == null) {
			throw new IllegalArgumentException("The passed object can not be null.");
		}
	}

	public static <T> void validatePassedObject(final Long id, final T object) {
		validatePassedObject(id);
		validatePassedObject(object);
	}
}
