package com.bluebik.car.service.musclecar;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluebik.car.core.AbstractExceptionHandler;
import com.bluebik.car.repository.MuscleCarRepository;
import com.bluebik.car.repository.domain.MuscleCar;

/**
 * Copyright © 2016 Bluebik Group. Created by khakhanat on 24/10/2017 AD.
 */
@Service
@Transactional(readOnly = true)
public class MuscleCarServiceImpl extends AbstractExceptionHandler implements MuscleCarService {

	@Autowired
	public MuscleCarRepository muscleCarRepositoty;

	public MuscleCar getCar(Long id) {
		validatePassedObject(id);
		return muscleCarRepositoty.findOne(id);
	}

	@Transactional
	public void removeCarFromList(Long id) {
		validatePassedObject(id);
		checkResourceFound(this.muscleCarRepositoty.findOne(id));
		muscleCarRepositoty.delete(id);
	}

	public List<MuscleCar> getAllCars() {
		List<MuscleCar> result = muscleCarRepositoty.findAll();
		return !result.isEmpty() ? result : null;
	}

	@Transactional
	public MuscleCar addCarToList(MuscleCar muscleCar) {
		validatePassedObject(muscleCar);
		return muscleCarRepositoty.save(muscleCar);
	}

	@Transactional
	public MuscleCar updateCarFromList(Long id, MuscleCar muscleCar) {
		validatePassedObject(id,muscleCar);
		checkResourceFound(this.muscleCarRepositoty.findOne(id));
		return muscleCarRepositoty.save(muscleCar);
	}

}
