package com.bluebik.car.service.musclecar;

import java.util.List;

import com.bluebik.car.repository.domain.MuscleCar;

public interface MuscleCarService {
	
	public MuscleCar getCar(Long id);
	
	public void removeCarFromList(Long id);
	
	public List<MuscleCar> getAllCars();
	
	public MuscleCar addCarToList(MuscleCar muscleCar);
	
	public MuscleCar updateCarFromList(Long id, MuscleCar muscleCar);
}
