package com.bluebik.car.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bluebik.car.repository.domain.MuscleCar;

/**
 * In my opinion, if you have created new table in database. this repository and
 * domain will be automatically generated from some application.
 * Auto Generated
 */

@Repository
public interface MuscleCarRepository extends JpaRepository<MuscleCar, Long> {

}
