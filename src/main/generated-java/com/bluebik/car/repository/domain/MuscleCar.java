package com.bluebik.car.repository.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Auto Generated
 */

@Entity
@Table(name = "muscle_car")
public class MuscleCar {

	@Id
	@GeneratedValue()
	private Long carId;

	@Column(nullable = false)
	private String carBrand;

	@Column(nullable = false)
	private String carModel;

	@Column(nullable = false)
	private String horsepower;

	@Column(nullable = false)
	private String carEngine;

	public MuscleCar() {

	}

	public MuscleCar(String carBrand, String carModel, String horsepower, String carEngine) {
		this.carBrand = carBrand;
		this.carModel = carModel;
		this.horsepower = horsepower;
		this.carEngine = carEngine;
	}

	public String getCarBrand() {
		return carBrand;
	}

	public void setCarBrand(String carBrand) {
		this.carBrand = carBrand;
	}

	public String getCarModel() {
		return carModel;
	}

	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}

	public String getHorsepower() {
		return horsepower;
	}

	public void setHorsepower(String horsepower) {
		this.horsepower = horsepower;
	}

	public String getCarEngine() {
		return carEngine;
	}

	public void setCarEngine(String carEngine) {
		this.carEngine = carEngine;
	}

	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId) {
		this.carId = carId;
	}

}
